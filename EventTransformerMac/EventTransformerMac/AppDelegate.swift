//
//  AppDelegate.swift
//  EventTransformerMac
//
//  Created by RentonLIn on 2018/10/26.
//  Copyright © 2018 RentonLIn. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

