//
//  ViewController.swift
//  EventTransformerMac
//
//  Created by RentonLIn on 2018/10/26.
//  Copyright © 2018 RentonLIn. All rights reserved.
//

import Cocoa

class ViewController: NSViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //read from file get event names
        let url = Bundle.main.url(forResource: "EventList", withExtension: "json")
        do {
            let nubmersCharSet = CharacterSet(charactersIn: "0123456789")
            let string = try NSString(contentsOf: url!, encoding: 4)
            let names = string.components(separatedBy: "\n")
            //map to "case OriginEvent = originEvent"
            let output = names.compactMap { (name) -> String? in
                guard !name.isEmpty else {
                    return nil
                }
                guard let firstCharacter = name.first,
                 nubmersCharSet.intersection(CharacterSet(charactersIn: String(firstCharacter))).isEmpty else {
                    return nil
                }
                
                let parsed = name.components(separatedBy: "_").map({ (string) -> String in
                    return string.getFirstCharacterUpcase()
                }).joined(separator: "")
                
                return "case \(parsed.getFirstCharacterLowercase()) = \"\(name)\""
            }
            //output to file
            print(output.joined(separator: "\n"))
        } catch {
            print("fail to parse source")
        }
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }


}

extension String {
    func getFirstCharacterUpcase() -> String {
        if self.count == 0 {
            return self
        } else {
            let firstCharacter = String(self.prefix(1))
            let leftCharacters = String(self.suffix(self.count - 1))
            return firstCharacter.uppercased() + leftCharacters
        }
    }
    
    func getFirstCharacterLowercase() -> String {
        if self.count == 0 {
            return self
        } else {
            let firstCharacter = String(self.prefix(1))
            let leftCharacters = String(self.suffix(self.count - 1))
            return firstCharacter.lowercased() + leftCharacters
        }
    }
}
