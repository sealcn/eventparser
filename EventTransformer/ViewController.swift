//
//  ViewController.swift
//  EventTransformer
//
//  Created by Renton on 2018/6/28.
//  Copyright © 2018 DAILYINNOVATION CO., LIMITED. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //read from file get event names
        let url = Bundle.main.url(forResource: "EventList", withExtension: "json")
        do {
            let string = try NSString(contentsOf: url!, encoding: 4)
            let names = string.components(separatedBy: "\n")
            //map to "case OriginEvent = originEvent"
            let output = names.map { (name) -> String in
                let parsed = name.components(separatedBy: "_").map({ (string) -> String in
                    return string.getFirstCharacterUpcase()
                }).joined(separator: "")
                
                return "case \(parsed.getFirstCharacterLowercase()) = \"\(name)\""
            }
            //output to file
            print(output.joined(separator: "\n"))
        } catch {
            print("fail to parse source")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension String {
    func getFirstCharacterUpcase() -> String {
        if self.count == 0 {
            return self
        } else {
            let firstCharacter = String(self.prefix(1))
            let leftCharacters = String(self.suffix(self.count - 1))
            return firstCharacter.uppercased() + leftCharacters
        }
    }
    
    func getFirstCharacterLowercase() -> String {
        if self.count == 0 {
            return self
        } else {
            let firstCharacter = String(self.prefix(1))
            let leftCharacters = String(self.suffix(self.count - 1))
            return firstCharacter.lowercased() + leftCharacters
        }
    }
}
